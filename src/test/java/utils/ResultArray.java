package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.appium.java_client.TouchAction;

public class ResultArray {
	
	static WebDriver we;
	
	static FileInputStream fi;
	static FileOutputStream fo;
	static XSSFWorkbook wb;
	static XSSFSheet sh;
	TouchAction tc;
	static int [] total,runningTC,passTestcase,failTestcase;
	static int p=0,q=0,r=0,s=0;
	public ResultArray(int count3, int z) {
		// TODO Auto-generated constructor stub
	}

		
	public static void Write_Into(String excel) throws Exception {
		Thread.sleep(4000);
		fi.close();
		fo = new FileOutputStream(excel);
		wb.write(fo);
		Thread.sleep(3000);
		fo.close();
	}
	
	public static String Read_In(int r, int c) {
		return (sh.getRow(r).getCell(c).getStringCellValue());
	}
		
	
	public static int[] passResult(WebDriver we,String sheetname,int c,
			int count3,String tcId,String pass[],int z,String excel,FileInputStream fi,XSSFWorkbook wb,XSSFSheet sh) 
			throws Exception{
		Thread.sleep(1000);
		
		wb.getSheet(sheetname).getRow(c).createCell(3).setCellValue("Pass");
		Thread.sleep(1000);
		count3++;
		System.out.println("Pass Test case id : " + tcId);
		System.out.println("count of pass test case : " + count3);
		Thread.sleep(1000);
		pass[z] = tcId;
		//System.out.println("pass test case id : " + pass[z]);
		//System.out.println("counter of array : " + z);
		z++;
		
		return new int []{count3,z};
		
	}
	
	
	public static int[] failResult(WebDriver we,String sheetname,int c,int count4,
			String tcId,String fail[],int f,Throwable s,String excel,FileInputStream fi,XSSFWorkbook wb,XSSFSheet sh) 
					throws Exception{
		System.out.println("failed" +c+ ": '" + s.getMessage() + "'");
		wb.getSheet(sheetname).getRow(c).createCell(3).setCellValue("Failed : '" + s.getMessage() + "'");
		Thread.sleep(1000);
		try{
		CommonUtils.getscreenshot(we,sheetname,tcId);
		Thread.sleep(1000);
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		
		Thread.sleep(1000);
		count4++;
		System.out.println("Failed Test case id : " + tcId);
		System.out.println("count of failed test case : " + count4);
		Thread.sleep(1000);
		fail[f] = tcId;
		//System.out.println("failed test case id : " + fail[f]);
		//System.out.println("counter of failed array : " + f);
		f++;
		Thread.sleep(1000);
	//	CommonUtils.goback(we);
		
		return new int[]{count4,f};
	}
	
	
	
	public static void result(WebDriver we,String excel,String name,String pass[],String fail[],int count1, int count2,int count3,int count4,int f,int z) throws Exception {
		Thread.sleep(1000);
		fi = new FileInputStream(excel);
		wb = new XSSFWorkbook(fi);
		sh = wb.getSheet("final_result");

		String descr1;
	    
		//System.out.println("value of p q r s before" +p+ q+r+s);

		// System.out.println(name+ ": Automated test cases for  : "+name+":"+count1);
		wb.getSheet("final_result").getRow(1).createCell(0)
				.setCellValue(name+ ": Automated test cases for  "+name+":"+count1);
		/*total[p]=count1;
		p++;
		System.out.println(" data of array total " +  Arrays.toString(total));*/
		Thread.sleep(1000);

		// System.out.println("Executing test cases of  : "+name+" : " + count2);
		wb.getSheet("final_result").getRow(2).createCell(0)
				.setCellValue(" : Executing test cases of  "+name+":"+count2);
		/*runningTC[q]= count2;
		q++;
		System.out.println(" data of array running " +  Arrays.toString(runningTC));*/
		Thread.sleep(1000);

		// System.out.println("Passed test cases of  :"+name+" : " + count3);
		wb.getSheet("final_result").getRow(3).createCell(0)
				.setCellValue(" : Passed test cases of  "+name+":"+count3);
		/*passTestcase[r]=count3;
		r++;
		System.out.println(" data of array passssssss " +  Arrays.toString(passTestcase));*/
		Thread.sleep(1000);

		// System.out.println("Failed test cases of  :"+name+" : " + count4);
		wb.getSheet("final_result").getRow(4).createCell(0)
				.setCellValue(" : Failed test cases of  "+name+":"+count4);
		/*failTestcase[s] =count4;
		s++;
		System.out.println(" data of array failll " +  Arrays.toString(failTestcase));*/
		Thread.sleep(1000);

		//System.out.println("value of p q r s after" +p+ q+r+s);
		
		sh = wb.getSheet("PassResult");
		String descr2;
		// System.out.println("current value of z : " + z);
		for (int i = 0; i < z; i++) {
			// System.out.println("Test case id of pass "+name+" test cases  : " + pass[i]);
			wb.getSheet("PassResult").getRow(i + 1).createCell(0)
					.setCellValue("Test case id of pass "+name+" test cases : " + pass[i]);
		}

		sh = wb.getSheet("FailedResult");
		Thread.sleep(1000);
		String descr3;
		we.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// System.out.println("current value of f : " + f);
		for (int j = 0; j < f; j++) {
			//System.out.println("Test case id of failed "+name+" test cases  : " + fail[j]);
			// System.out.println(name+" failed test cases ID : "+ fail[j]);
			wb.getSheet("FailedResult").getRow(j + 1).createCell(0)
					.setCellValue(name+" failed test cases ID : "+ fail[j]);
		}
		Thread.sleep(2000);
		Write_Into(excel);
		Thread.sleep(1000);
	}

	public static String[] ArrayPrint1(String excel) throws InterruptedException, IOException {
		Thread.sleep(1000);
		
		fi = new FileInputStream(excel);
		wb = new XSSFWorkbook(fi);

		int cnt1 = 0;
		int rowCount1 = 0;

		sh = wb.getSheet("final_result");
		for (int m = 1; m <= sh.getLastRowNum(); m++) {
			String descr11 = Read_In(m, 0);
			if (!descr11.equals("")) {
				// System.out.println("Descr1 :" + descr11 + " & count : " + rowCount1);
				rowCount1++;
			}
		}
		Thread.sleep(1000);

		String arrayDesc1[] = new String[rowCount1];

		sh = wb.getSheet("final_result");
		for (int m = 1; m <= rowCount1; m++) {
			String descr11 = Read_In(m, 0);
			cnt1++;
			arrayDesc1[m - 1] = descr11;
			// System.out.println("Array value 1 : " + arrayDesc1[m - 1]);
		}

		return arrayDesc1;
	}

	public static String[] ArrayPrint2(String excel) throws InterruptedException, IOException {
		Thread.sleep(1000);
		//String excel = "D:\\selenium\\newExcel\\Search.xlsx";
		fi = new FileInputStream(excel);
		wb = new XSSFWorkbook(fi);

		int cnt2 = 0;
		int rowCount2 = 0;

		Thread.sleep(1000);
		sh = wb.getSheet("PassResult");
		for (int m = 1; m <= sh.getLastRowNum(); m++) {
			String descr22 = Read_In(m, 0);
			if (!descr22.equals("")) {
				// System.out.println("Descr2 :" + descr22 + " & count : " + rowCount2);
				rowCount2++;
			}
		}
		Thread.sleep(1000);

		String arrayDesc2[] = new String[rowCount2];

		sh = wb.getSheet("PassResult");
		for (int m = 1; m <= rowCount2; m++) {
			String descr22 = Read_In(m, 0);
			cnt2++;
			arrayDesc2[m - 1] = descr22;
			// System.out.println("Array value 2 : " + arrayDesc2[m - 1]);
		}

		return arrayDesc2;
	}

	public static String[] ArrayPrint3(String excel) throws InterruptedException, IOException {
		Thread.sleep(1000);
		//String excel = "D:\\selenium\\newExcel\\Search.xlsx";
		fi = new FileInputStream(excel);
		wb = new XSSFWorkbook(fi);

		int cnt3 = 0;
		int rowCount3 = 0;

		Thread.sleep(1000);
		sh = wb.getSheet("FailedResult");
		for (int m = 1; m <= sh.getLastRowNum(); m++) {
			String descr33 = Read_In(m, 0);
			if (!descr33.equals("")) {
				// System.out.println("Descr3 :" + descr33 + " & count : " + rowCount3);
				rowCount3++;
			}
		}

		String arrayDesc3[] = new String[rowCount3];

		sh = wb.getSheet("FailedResult");
		for (int m = 1; m <= rowCount3; m++) {
			String descr33 = Read_In(m, 0);
			cnt3++;
			arrayDesc3[m - 1] = descr33;
			// System.out.println("Array value 3 : " + arrayDesc3[m - 1]);
		}

		return arrayDesc3;
	}
	
	
	public static void end(WebDriver we,String excel,String name,String pass[],String fail[],int count1, int count2,int count3,int count4,int f,int z) throws Exception{
		
		Thread.sleep(1000);
		result(we, excel, name, pass, fail, count1, count2,count3,count4,f, z);
		Thread.sleep(1000);
		ArrayPrint1(excel);
		Thread.sleep(1000);
		ArrayPrint2(excel);
		Thread.sleep(1000);
		ArrayPrint3(excel);
		Thread.sleep(1000);
		we.quit();
	}
}