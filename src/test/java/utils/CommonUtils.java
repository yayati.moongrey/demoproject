package utils;

import static org.testng.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;

import io.appium.java_client.android.AndroidDriver;
import xpaths.Path;

public class CommonUtils {

	static Actions ac;
	public static String nameOfDirectory(String excelname) {
		
		  String resourcePath= "/src/test/java/resources/";
		  
	       //String excelname = "Search.xlsx";
	       String dir = "";
	       dir = System.getProperty("user.dir") + resourcePath +excelname;
	       dir = dir.replace("\\", "/");
	       System.out.println("path ===== "+ dir);
		return dir;
	}
	
	public static void mousehoverClick(WebDriver we,WebElement ele,String colorCd) throws InterruptedException{
		ac=new Actions(we);
		ac.moveToElement(ele).click().build().perform();Thread.sleep(2000);
		String color = ele.getCssValue("color");
		String hex = Color.fromString(color).asHex();
		if(colorCd.equals(hex)){
			System.out.println("color after clicking : "+hex);
		}
		else{
			System.out.println("color after clicking : "+hex+ " & actual color we have : "+colorCd);
		}
	Thread.sleep(1000);
	}
	
	public static void mousehover(WebDriver we,WebElement ele,String colorCd) throws InterruptedException{
		ac=new Actions(we);
		ac.moveToElement(ele).build().perform();Thread.sleep(2000);
		String color = ele.getCssValue("color");
		String hex = Color.fromString(color).asHex();
		if(colorCd.equals(hex)){
			System.out.println("color after clicking : "+hex);
		}
		else{
			System.out.println("color after clicking : "+hex+ " & actual color we have : "+colorCd);
		}
	Thread.sleep(1000);
	}
	

//Method to remove alerts from the web page
public void purgeAllAlerts(WebDriver we) {
    try {
        //Thread.sleep(purgeInterval);
        Alert alert = we.switchTo().alert();
        if (alert != null)
            alert.dismiss();
    } catch (Exception ex) {
        // Intentionally left blank.
    }
}


	//////////////////////////////--------- SCREENSHOT ----------/////////////////////////////////////
	
	public static void getscreenshot(WebDriver we,String sheetName,String tcId) throws IOException
	{
	
	System.out.println("Taking screenshot");
	SimpleDateFormat sdf=new SimpleDateFormat("dd_MM_yyy_hh_mm_ss");
	Date date=new Date();
	String DateTime=sdf.format(date);
	
	File filesrc=((RemoteWebDriver) we).getScreenshotAs(OutputType.FILE);
	
	FileUtils.copyFile(filesrc, new File(System.getProperty("user.dir")+"//Screenshot//"+sheetName+"--"+tcId+"--"+DateTime+".png"));
	System.out.println("Screenshot captured");
	}
	
	
//////////////////////////////////////////////////////////////////////////	
	
	
	// Create RANDOM Alphanumeric String
		public static String getRandomEmail(int n) {

			// chose a Character random from this String
			String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

			// create StringBuffer size of AlphaNumericString
			StringBuilder sb = new StringBuilder(n);

			for (int i = 0; i < n; i++) {

				// generate a random number between
				// 0 to AlphaNumericString variable length
				int index = (int) (AlphaNumericString.length() * Math.random());

				// add Character one by one in end of sb
				sb.append(AlphaNumericString.charAt(index));
			}

			return sb.toString();
		}
	
		public static String getRandomUser(int n) {

			// chose a Character random from this String
			String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

			// create StringBuffer size of AlphaNumericString
			StringBuilder sb = new StringBuilder(n);

			for (int i = 0; i < n; i++) {

				// generate a random number between
				// 0 to AlphaNumericString variable length
				int index = (int) (AlphaNumericString.length() * Math.random());

				// add Character one by one in end of sb
				sb.append(AlphaNumericString.charAt(index));
			}

			return sb.toString();
		}
	
	public static XSSFSheet ReadExcel(FileInputStream fi,XSSFWorkbook wb,XSSFSheet sh,String excel,String sheetname) throws IOException{
		fi = new FileInputStream(excel);
		wb = new XSSFWorkbook(fi);
		sh = wb.getSheet(sheetname);
		
		return sh;
	}

}