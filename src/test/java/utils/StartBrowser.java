package utils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import config.DataProvider;
import xpaths.Path;

public class StartBrowser {
	
	//static WebDriver we;
	
	public static String url= DataProvider.BaseUrl;
	public static String chromedriver = "chromedriver.exe";
	public static String iedriver = "IEDriverServer.exe";
	public static String firefoxdriver="firefox-sdk\\bin\\firefox.exe";
	
  public static WebDriver getBrowser(WebDriver we,String browser) throws InterruptedException {
	 
	  if(browser.equalsIgnoreCase("chrome")){
		  System.setProperty("webdriver.chrome.driver", CommonUtils.nameOfDirectory(chromedriver));
		  we=new ChromeDriver();
		  System.out.println("chrome driver value:" +we);
		System.out.println("Chrome browser open");  
	  }
	  
	  if(browser.equalsIgnoreCase("firefox")){
		  System.setProperty("webdriver.firefox.bin", CommonUtils.nameOfDirectory(firefoxdriver));
		  we=new FirefoxDriver();
		System.out.println("firefox browser open");  
	  }

	  if(browser.equalsIgnoreCase("IE")){
			
		  System.setProperty("webdriver.ie.driver", CommonUtils.nameOfDirectory(iedriver));
		  we=new InternetExplorerDriver(); 
		 /* DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
		  caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "");
		  we = new InternetExplorerDriver();*/
		  System.out.println("ie driver value:" +we);
		 // we.manage().window().maximize(); Thread.sleep(5000);
		System.out.println("IE browser open");  
		
	  }
	  
	 Thread.sleep(3000);
	  we.get(url);
	  Thread.sleep(5000);
	 // System.out.println("Title of the current loaded page : "+we.getTitle());
	  we.manage().window().maximize();
	 
	  Thread.sleep(3000);
	
	 // we.navigate().refresh();
	 
	        try{
	        	WebElement alertBox=we.findElement(By.xpath(Path.AlertDismissX));
	        	alertBox.click();
		        Thread.sleep(2000);
		        System.out.println("clicked on no thanx ");
	        }
	        catch(Exception e){
	        	System.out.println("No alert box is open");
	        }
	return we;

  }
  
	
  
  
}
