package xpaths;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class Path {
	
	public static String AlertDismissX = "";
	
	public static String searchbox = "//input[@id='search_query_top']";
	public static String search_submit_button = "//button[@name='submit_search']";
	
	public static String search_results_search_text_heading= "//h1[contains(@class,'product-listing')]";
	public static String search_result_counts = "//span[@class='heading-counter']";
	public static String search_please_enter_keyword_alert = "//p[@class='alert alert-warning']";
	
	public static String category_women= "Women";
	public static String category_dresses = "//body[@id='search']/div[@id='page']/div[@class='header-container']/header[@id='header']/div/div[@class='container']/div[@class='row']/div[@id='block_top_menu']/ul[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']/li[2]/a[1]";
	public static String category_T_shirts = "//body[@id='search']/div[@id='page']/div[@class='header-container']/header[@id='header']/div/div[@class='container']/div[@class='row']/div[@id='block_top_menu']/ul[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']/li[3]/a[1]";
	
	
	
}