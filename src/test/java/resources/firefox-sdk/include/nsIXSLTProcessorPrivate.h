/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM ../../../dist/idl\nsIXSLTProcessorPrivate.idl
 */

#ifndef __gen_nsIXSLTProcessorPrivate_h__
#define __gen_nsIXSLTProcessorPrivate_h__


#ifndef __gen_nsISupports_h__
#include "nsISupports.h"
#endif

/* For IDL files that don't want to include root IDL files. */
#ifndef NS_NO_VTABLE
#define NS_NO_VTABLE
#endif

/* starting interface:    nsIXSLTProcessorPrivate */
#define NS_IXSLTPROCESSORPRIVATE_IID_STR "75d14f5d-293d-4872-8a26-e79268de592f"

#define NS_IXSLTPROCESSORPRIVATE_IID \
  {0x75d14f5d, 0x293d, 0x4872, \
    { 0x8a, 0x26, 0xe7, 0x92, 0x68, 0xde, 0x59, 0x2f }}

class NS_NO_VTABLE nsIXSLTProcessorPrivate : public nsISupports {
 public:

  NS_DECLARE_STATIC_IID_ACCESSOR(NS_IXSLTPROCESSORPRIVATE_IID)

  /* void init (in nsISupports global); */
  NS_IMETHOD Init(nsISupports *global) = 0;

  enum {
    DISABLE_ALL_LOADS = 1U
  };

  /* attribute unsigned long flags; */
  NS_IMETHOD GetFlags(uint32_t *aFlags) = 0;
  NS_IMETHOD SetFlags(uint32_t aFlags) = 0;

};

  NS_DEFINE_STATIC_IID_ACCESSOR(nsIXSLTProcessorPrivate, NS_IXSLTPROCESSORPRIVATE_IID)

/* Use this macro when declaring classes that implement this interface. */
#define NS_DECL_NSIXSLTPROCESSORPRIVATE \
  NS_IMETHOD Init(nsISupports *global) override; \
  NS_IMETHOD GetFlags(uint32_t *aFlags) override; \
  NS_IMETHOD SetFlags(uint32_t aFlags) override; 

/* Use this macro when declaring the members of this interface when the
   class doesn't implement the interface. This is useful for forwarding. */
#define NS_DECL_NON_VIRTUAL_NSIXSLTPROCESSORPRIVATE \
  NS_METHOD Init(nsISupports *global); \
  NS_METHOD GetFlags(uint32_t *aFlags); \
  NS_METHOD SetFlags(uint32_t aFlags); 

/* Use this macro to declare functions that forward the behavior of this interface to another object. */
#define NS_FORWARD_NSIXSLTPROCESSORPRIVATE(_to) \
  NS_IMETHOD Init(nsISupports *global) override { return _to Init(global); } \
  NS_IMETHOD GetFlags(uint32_t *aFlags) override { return _to GetFlags(aFlags); } \
  NS_IMETHOD SetFlags(uint32_t aFlags) override { return _to SetFlags(aFlags); } 

/* Use this macro to declare functions that forward the behavior of this interface to another object in a safe way. */
#define NS_FORWARD_SAFE_NSIXSLTPROCESSORPRIVATE(_to) \
  NS_IMETHOD Init(nsISupports *global) override { return !_to ? NS_ERROR_NULL_POINTER : _to->Init(global); } \
  NS_IMETHOD GetFlags(uint32_t *aFlags) override { return !_to ? NS_ERROR_NULL_POINTER : _to->GetFlags(aFlags); } \
  NS_IMETHOD SetFlags(uint32_t aFlags) override { return !_to ? NS_ERROR_NULL_POINTER : _to->SetFlags(aFlags); } 

#if 0
/* Use the code below as a template for the implementation class for this interface. */

/* Header file */
class nsXSLTProcessorPrivate : public nsIXSLTProcessorPrivate
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_NSIXSLTPROCESSORPRIVATE

  nsXSLTProcessorPrivate();

private:
  ~nsXSLTProcessorPrivate();

protected:
  /* additional members */
};

/* Implementation file */
NS_IMPL_ISUPPORTS(nsXSLTProcessorPrivate, nsIXSLTProcessorPrivate)

nsXSLTProcessorPrivate::nsXSLTProcessorPrivate()
{
  /* member initializers and constructor code */
}

nsXSLTProcessorPrivate::~nsXSLTProcessorPrivate()
{
  /* destructor code */
}

/* void init (in nsISupports global); */
NS_IMETHODIMP nsXSLTProcessorPrivate::Init(nsISupports *global)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute unsigned long flags; */
NS_IMETHODIMP nsXSLTProcessorPrivate::GetFlags(uint32_t *aFlags)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP nsXSLTProcessorPrivate::SetFlags(uint32_t aFlags)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* End of implementation class template. */
#endif


#endif /* __gen_nsIXSLTProcessorPrivate_h__ */
