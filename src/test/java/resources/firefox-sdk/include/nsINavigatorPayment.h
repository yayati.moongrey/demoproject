/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM ../../../dist/idl\nsINavigatorPayment.idl
 */

#ifndef __gen_nsINavigatorPayment_h__
#define __gen_nsINavigatorPayment_h__


#ifndef __gen_domstubs_h__
#include "domstubs.h"
#endif

#include "js/Value.h"

/* For IDL files that don't want to include root IDL files. */
#ifndef NS_NO_VTABLE
#define NS_NO_VTABLE
#endif
class nsIDOMDOMRequest; /* forward declaration */


/* starting interface:    nsINavigatorPayment */
#define NS_INAVIGATORPAYMENT_IID_STR "44fb7308-7d7b-4975-8a27-e01fe9623bdb"

#define NS_INAVIGATORPAYMENT_IID \
  {0x44fb7308, 0x7d7b, 0x4975, \
    { 0x8a, 0x27, 0xe0, 0x1f, 0xe9, 0x62, 0x3b, 0xdb }}

class NS_NO_VTABLE nsINavigatorPayment : public nsISupports {
 public:

  NS_DECLARE_STATIC_IID_ACCESSOR(NS_INAVIGATORPAYMENT_IID)

  /* nsIDOMDOMRequest pay (in jsval jwts); */
  NS_IMETHOD Pay(JS::HandleValue jwts, nsIDOMDOMRequest * *_retval) = 0;

};

  NS_DEFINE_STATIC_IID_ACCESSOR(nsINavigatorPayment, NS_INAVIGATORPAYMENT_IID)

/* Use this macro when declaring classes that implement this interface. */
#define NS_DECL_NSINAVIGATORPAYMENT \
  NS_IMETHOD Pay(JS::HandleValue jwts, nsIDOMDOMRequest * *_retval) override; 

/* Use this macro when declaring the members of this interface when the
   class doesn't implement the interface. This is useful for forwarding. */
#define NS_DECL_NON_VIRTUAL_NSINAVIGATORPAYMENT \
  NS_METHOD Pay(JS::HandleValue jwts, nsIDOMDOMRequest * *_retval); 

/* Use this macro to declare functions that forward the behavior of this interface to another object. */
#define NS_FORWARD_NSINAVIGATORPAYMENT(_to) \
  NS_IMETHOD Pay(JS::HandleValue jwts, nsIDOMDOMRequest * *_retval) override { return _to Pay(jwts, _retval); } 

/* Use this macro to declare functions that forward the behavior of this interface to another object in a safe way. */
#define NS_FORWARD_SAFE_NSINAVIGATORPAYMENT(_to) \
  NS_IMETHOD Pay(JS::HandleValue jwts, nsIDOMDOMRequest * *_retval) override { return !_to ? NS_ERROR_NULL_POINTER : _to->Pay(jwts, _retval); } 

#if 0
/* Use the code below as a template for the implementation class for this interface. */

/* Header file */
class nsNavigatorPayment : public nsINavigatorPayment
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_NSINAVIGATORPAYMENT

  nsNavigatorPayment();

private:
  ~nsNavigatorPayment();

protected:
  /* additional members */
};

/* Implementation file */
NS_IMPL_ISUPPORTS(nsNavigatorPayment, nsINavigatorPayment)

nsNavigatorPayment::nsNavigatorPayment()
{
  /* member initializers and constructor code */
}

nsNavigatorPayment::~nsNavigatorPayment()
{
  /* destructor code */
}

/* nsIDOMDOMRequest pay (in jsval jwts); */
NS_IMETHODIMP nsNavigatorPayment::Pay(JS::HandleValue jwts, nsIDOMDOMRequest * *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* End of implementation class template. */
#endif


#endif /* __gen_nsINavigatorPayment_h__ */
