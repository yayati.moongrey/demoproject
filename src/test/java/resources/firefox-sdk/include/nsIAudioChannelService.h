/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM ../../../dist/idl\nsIAudioChannelService.idl
 */

#ifndef __gen_nsIAudioChannelService_h__
#define __gen_nsIAudioChannelService_h__


#ifndef __gen_nsISupports_h__
#include "nsISupports.h"
#endif

/* For IDL files that don't want to include root IDL files. */
#ifndef NS_NO_VTABLE
#define NS_NO_VTABLE
#endif
class nsIDOMWindow; /* forward declaration */


/* starting interface:    nsIAudioChannelService */
#define NS_IAUDIOCHANNELSERVICE_IID_STR "323e5472-b8f4-4288-b1b9-53c7c54bbbe8"

#define NS_IAUDIOCHANNELSERVICE_IID \
  {0x323e5472, 0xb8f4, 0x4288, \
    { 0xb1, 0xb9, 0x53, 0xc7, 0xc5, 0x4b, 0xbb, 0xe8 }}

class NS_NO_VTABLE nsIAudioChannelService : public nsISupports {
 public:

  NS_DECLARE_STATIC_IID_ACCESSOR(NS_IAUDIOCHANNELSERVICE_IID)

  /* float getAudioChannelVolume (in nsIDOMWindow window, in unsigned short audioChannel); */
  NS_IMETHOD GetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float *_retval) = 0;

  /* void setAudioChannelVolume (in nsIDOMWindow window, in unsigned short audioChannel, in float volume); */
  NS_IMETHOD SetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float volume) = 0;

  /* boolean getAudioChannelMuted (in nsIDOMWindow window, in unsigned short audioChannel); */
  NS_IMETHOD GetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) = 0;

  /* void setAudioChannelMuted (in nsIDOMWindow window, in unsigned short audioChannel, in boolean muted); */
  NS_IMETHOD SetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool muted) = 0;

  /* boolean isAudioChannelActive (in nsIDOMWindow window, in unsigned short audioChannel); */
  NS_IMETHOD IsAudioChannelActive(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) = 0;

};

  NS_DEFINE_STATIC_IID_ACCESSOR(nsIAudioChannelService, NS_IAUDIOCHANNELSERVICE_IID)

/* Use this macro when declaring classes that implement this interface. */
#define NS_DECL_NSIAUDIOCHANNELSERVICE \
  NS_IMETHOD GetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float *_retval) override; \
  NS_IMETHOD SetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float volume) override; \
  NS_IMETHOD GetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) override; \
  NS_IMETHOD SetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool muted) override; \
  NS_IMETHOD IsAudioChannelActive(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) override; 

/* Use this macro when declaring the members of this interface when the
   class doesn't implement the interface. This is useful for forwarding. */
#define NS_DECL_NON_VIRTUAL_NSIAUDIOCHANNELSERVICE \
  NS_METHOD GetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float *_retval); \
  NS_METHOD SetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float volume); \
  NS_METHOD GetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval); \
  NS_METHOD SetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool muted); \
  NS_METHOD IsAudioChannelActive(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval); 

/* Use this macro to declare functions that forward the behavior of this interface to another object. */
#define NS_FORWARD_NSIAUDIOCHANNELSERVICE(_to) \
  NS_IMETHOD GetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float *_retval) override { return _to GetAudioChannelVolume(window, audioChannel, _retval); } \
  NS_IMETHOD SetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float volume) override { return _to SetAudioChannelVolume(window, audioChannel, volume); } \
  NS_IMETHOD GetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) override { return _to GetAudioChannelMuted(window, audioChannel, _retval); } \
  NS_IMETHOD SetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool muted) override { return _to SetAudioChannelMuted(window, audioChannel, muted); } \
  NS_IMETHOD IsAudioChannelActive(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) override { return _to IsAudioChannelActive(window, audioChannel, _retval); } 

/* Use this macro to declare functions that forward the behavior of this interface to another object in a safe way. */
#define NS_FORWARD_SAFE_NSIAUDIOCHANNELSERVICE(_to) \
  NS_IMETHOD GetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float *_retval) override { return !_to ? NS_ERROR_NULL_POINTER : _to->GetAudioChannelVolume(window, audioChannel, _retval); } \
  NS_IMETHOD SetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float volume) override { return !_to ? NS_ERROR_NULL_POINTER : _to->SetAudioChannelVolume(window, audioChannel, volume); } \
  NS_IMETHOD GetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) override { return !_to ? NS_ERROR_NULL_POINTER : _to->GetAudioChannelMuted(window, audioChannel, _retval); } \
  NS_IMETHOD SetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool muted) override { return !_to ? NS_ERROR_NULL_POINTER : _to->SetAudioChannelMuted(window, audioChannel, muted); } \
  NS_IMETHOD IsAudioChannelActive(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval) override { return !_to ? NS_ERROR_NULL_POINTER : _to->IsAudioChannelActive(window, audioChannel, _retval); } 

#if 0
/* Use the code below as a template for the implementation class for this interface. */

/* Header file */
class nsAudioChannelService : public nsIAudioChannelService
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_NSIAUDIOCHANNELSERVICE

  nsAudioChannelService();

private:
  ~nsAudioChannelService();

protected:
  /* additional members */
};

/* Implementation file */
NS_IMPL_ISUPPORTS(nsAudioChannelService, nsIAudioChannelService)

nsAudioChannelService::nsAudioChannelService()
{
  /* member initializers and constructor code */
}

nsAudioChannelService::~nsAudioChannelService()
{
  /* destructor code */
}

/* float getAudioChannelVolume (in nsIDOMWindow window, in unsigned short audioChannel); */
NS_IMETHODIMP nsAudioChannelService::GetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void setAudioChannelVolume (in nsIDOMWindow window, in unsigned short audioChannel, in float volume); */
NS_IMETHODIMP nsAudioChannelService::SetAudioChannelVolume(nsIDOMWindow *window, uint16_t audioChannel, float volume)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* boolean getAudioChannelMuted (in nsIDOMWindow window, in unsigned short audioChannel); */
NS_IMETHODIMP nsAudioChannelService::GetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void setAudioChannelMuted (in nsIDOMWindow window, in unsigned short audioChannel, in boolean muted); */
NS_IMETHODIMP nsAudioChannelService::SetAudioChannelMuted(nsIDOMWindow *window, uint16_t audioChannel, bool muted)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* boolean isAudioChannelActive (in nsIDOMWindow window, in unsigned short audioChannel); */
NS_IMETHODIMP nsAudioChannelService::IsAudioChannelActive(nsIDOMWindow *window, uint16_t audioChannel, bool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* End of implementation class template. */
#endif


#endif /* __gen_nsIAudioChannelService_h__ */
