/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM ../../../dist/idl\nsIDOMMessageEvent.idl
 */

#ifndef __gen_nsIDOMMessageEvent_h__
#define __gen_nsIDOMMessageEvent_h__


#ifndef __gen_nsISupports_h__
#include "nsISupports.h"
#endif

#ifndef __gen_nsIDOMWindow_h__
#include "nsIDOMWindow.h"
#endif

#include "js/Value.h"

/* For IDL files that don't want to include root IDL files. */
#ifndef NS_NO_VTABLE
#define NS_NO_VTABLE
#endif

/* starting interface:    nsIDOMMessageEvent */
#define NS_IDOMMESSAGEEVENT_IID_STR "5d57bc56-30cf-4839-9e98-17f940120ec0"

#define NS_IDOMMESSAGEEVENT_IID \
  {0x5d57bc56, 0x30cf, 0x4839, \
    { 0x9e, 0x98, 0x17, 0xf9, 0x40, 0x12, 0x0e, 0xc0 }}

class NS_NO_VTABLE nsIDOMMessageEvent : public nsISupports {
 public:

  NS_DECLARE_STATIC_IID_ACCESSOR(NS_IDOMMESSAGEEVENT_IID)

  /* [implicit_jscontext] readonly attribute jsval data; */
  NS_IMETHOD GetData(JSContext* cx, JS::MutableHandleValue aData) = 0;

  /* readonly attribute DOMString origin; */
  NS_IMETHOD GetOrigin(nsAString & aOrigin) = 0;

  /* readonly attribute DOMString lastEventId; */
  NS_IMETHOD GetLastEventId(nsAString & aLastEventId) = 0;

  /* readonly attribute nsIDOMWindow source; */
  NS_IMETHOD GetSource(nsIDOMWindow * *aSource) = 0;

  /* void initMessageEvent (in DOMString aType, in boolean aCanBubble, in boolean aCancelable, in jsval aData, in DOMString aOrigin, in DOMString aLastEventId, in nsIDOMWindow aSource); */
  NS_IMETHOD InitMessageEvent(const nsAString & aType, bool aCanBubble, bool aCancelable, JS::HandleValue aData, const nsAString & aOrigin, const nsAString & aLastEventId, nsIDOMWindow *aSource) = 0;

};

  NS_DEFINE_STATIC_IID_ACCESSOR(nsIDOMMessageEvent, NS_IDOMMESSAGEEVENT_IID)

/* Use this macro when declaring classes that implement this interface. */
#define NS_DECL_NSIDOMMESSAGEEVENT \
  NS_IMETHOD GetData(JSContext* cx, JS::MutableHandleValue aData) override; \
  NS_IMETHOD GetOrigin(nsAString & aOrigin) override; \
  NS_IMETHOD GetLastEventId(nsAString & aLastEventId) override; \
  NS_IMETHOD GetSource(nsIDOMWindow * *aSource) override; \
  NS_IMETHOD InitMessageEvent(const nsAString & aType, bool aCanBubble, bool aCancelable, JS::HandleValue aData, const nsAString & aOrigin, const nsAString & aLastEventId, nsIDOMWindow *aSource) override; 

/* Use this macro when declaring the members of this interface when the
   class doesn't implement the interface. This is useful for forwarding. */
#define NS_DECL_NON_VIRTUAL_NSIDOMMESSAGEEVENT \
  NS_METHOD GetData(JSContext* cx, JS::MutableHandleValue aData); \
  NS_METHOD GetOrigin(nsAString & aOrigin); \
  NS_METHOD GetLastEventId(nsAString & aLastEventId); \
  NS_METHOD GetSource(nsIDOMWindow * *aSource); \
  NS_METHOD InitMessageEvent(const nsAString & aType, bool aCanBubble, bool aCancelable, JS::HandleValue aData, const nsAString & aOrigin, const nsAString & aLastEventId, nsIDOMWindow *aSource); 

/* Use this macro to declare functions that forward the behavior of this interface to another object. */
#define NS_FORWARD_NSIDOMMESSAGEEVENT(_to) \
  NS_IMETHOD GetData(JSContext* cx, JS::MutableHandleValue aData) override { return _to GetData(cx, aData); } \
  NS_IMETHOD GetOrigin(nsAString & aOrigin) override { return _to GetOrigin(aOrigin); } \
  NS_IMETHOD GetLastEventId(nsAString & aLastEventId) override { return _to GetLastEventId(aLastEventId); } \
  NS_IMETHOD GetSource(nsIDOMWindow * *aSource) override { return _to GetSource(aSource); } \
  NS_IMETHOD InitMessageEvent(const nsAString & aType, bool aCanBubble, bool aCancelable, JS::HandleValue aData, const nsAString & aOrigin, const nsAString & aLastEventId, nsIDOMWindow *aSource) override { return _to InitMessageEvent(aType, aCanBubble, aCancelable, aData, aOrigin, aLastEventId, aSource); } 

/* Use this macro to declare functions that forward the behavior of this interface to another object in a safe way. */
#define NS_FORWARD_SAFE_NSIDOMMESSAGEEVENT(_to) \
  NS_IMETHOD GetData(JSContext* cx, JS::MutableHandleValue aData) override { return !_to ? NS_ERROR_NULL_POINTER : _to->GetData(cx, aData); } \
  NS_IMETHOD GetOrigin(nsAString & aOrigin) override { return !_to ? NS_ERROR_NULL_POINTER : _to->GetOrigin(aOrigin); } \
  NS_IMETHOD GetLastEventId(nsAString & aLastEventId) override { return !_to ? NS_ERROR_NULL_POINTER : _to->GetLastEventId(aLastEventId); } \
  NS_IMETHOD GetSource(nsIDOMWindow * *aSource) override { return !_to ? NS_ERROR_NULL_POINTER : _to->GetSource(aSource); } \
  NS_IMETHOD InitMessageEvent(const nsAString & aType, bool aCanBubble, bool aCancelable, JS::HandleValue aData, const nsAString & aOrigin, const nsAString & aLastEventId, nsIDOMWindow *aSource) override { return !_to ? NS_ERROR_NULL_POINTER : _to->InitMessageEvent(aType, aCanBubble, aCancelable, aData, aOrigin, aLastEventId, aSource); } 

#if 0
/* Use the code below as a template for the implementation class for this interface. */

/* Header file */
class nsDOMMessageEvent : public nsIDOMMessageEvent
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_NSIDOMMESSAGEEVENT

  nsDOMMessageEvent();

private:
  ~nsDOMMessageEvent();

protected:
  /* additional members */
};

/* Implementation file */
NS_IMPL_ISUPPORTS(nsDOMMessageEvent, nsIDOMMessageEvent)

nsDOMMessageEvent::nsDOMMessageEvent()
{
  /* member initializers and constructor code */
}

nsDOMMessageEvent::~nsDOMMessageEvent()
{
  /* destructor code */
}

/* [implicit_jscontext] readonly attribute jsval data; */
NS_IMETHODIMP nsDOMMessageEvent::GetData(JSContext* cx, JS::MutableHandleValue aData)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute DOMString origin; */
NS_IMETHODIMP nsDOMMessageEvent::GetOrigin(nsAString & aOrigin)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute DOMString lastEventId; */
NS_IMETHODIMP nsDOMMessageEvent::GetLastEventId(nsAString & aLastEventId)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute nsIDOMWindow source; */
NS_IMETHODIMP nsDOMMessageEvent::GetSource(nsIDOMWindow * *aSource)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void initMessageEvent (in DOMString aType, in boolean aCanBubble, in boolean aCancelable, in jsval aData, in DOMString aOrigin, in DOMString aLastEventId, in nsIDOMWindow aSource); */
NS_IMETHODIMP nsDOMMessageEvent::InitMessageEvent(const nsAString & aType, bool aCanBubble, bool aCancelable, JS::HandleValue aData, const nsAString & aOrigin, const nsAString & aLastEventId, nsIDOMWindow *aSource)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* End of implementation class template. */
#endif


#endif /* __gen_nsIDOMMessageEvent_h__ */
