package reports;

import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ObjectArrays;
import io.appium.java_client.android.AndroidDriver;
import testcases.*;
import utils.*;

public class email {
	Properties mailServerProperties;
	Session getMailSession;
	MimeMessage generateMailMessage;
	AndroidDriver we;
	// String version="";

	public void generateAndSendEmail(String[] strs, String[] strs2)
			throws AddressException, MessagingException, MalformedURLException, InterruptedException {

		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();

		System.out.println(formatter.format(date));
		
		/////////////////////////////////////// New code with Testcases Data(Summary)///////////////////////////////
		String str = "", str1 = "";
		int len = strs.length;
		int tt = 0, tf = 0, tr = 0, tp = 0;
		String transfr[] = new String[len];
		Integer[] intarray = new Integer[len];
		int ii = 0, h;
		if (strs != null && strs.length > 0) {
			// This means there are some elements inside name array.
			System.out.println("summary array has elements" + Arrays.toString(strs));
			for (String s : strs) {
				String[] s2 = s.split(":");

				System.out.println("after split" + Arrays.toString(s2));
				/*
				 * for(String results : s2) { // System.out.println(results); }
				 */
				// str += "<tr><td>" + s2[0] + "</td><td>" + s2[1] + "</td></tr>";
				str += "<tr><td>" + s2[0] + "</td><td>" + s2[1] + "</td><td>" + s2[2] + "</td></tr>";
				String ss = s2[2];
				System.out.println("Value of array :" + ss);
				for (h = ii; h <= ii; h++) {
					// System.out.println("=================" +Arrays.toString(s2[2]));
					transfr[h] = ss;
					// ii++;
					System.out.println("=========================e : " + transfr[h]);
				}

				ii++;
			}

			int k = 0;
			for (String st : transfr) {
				intarray[k] = Integer.parseInt(st);// Exception in this line
				k++;
			}
			System.out.println("3rd column array value :String Array : " + Arrays.toString(transfr));
			System.out.println(str);
			System.out.println("Int array :::::::::;;" + Arrays.toString(intarray));

			int a = intarray.length;
			int[] t = new int[a];
			int[] r = new int[a];
			int[] p = new int[a];
			int[] f = new int[a];

			// int tt=0,tf=0,tr=0,tp=0;
			for (int i = 0; i < a; i = i + 4) {

				t[i] = intarray[i] + tt;
				tt = t[i];
				// t[i] = arr[i]+t[i];
				// System.out.println("value of array : "+t[i]);
			}
			System.out.println("value of array tt : " + tt);

			for (int i = 1; i < a; i = i + 4) {

				r[i] = intarray[i] + tr;
				tr = r[i];
				// t[i] = arr[i]+t[i];
				// System.out.println("value of array : "+r[i]);
			}
			System.out.println("value of array  tr: " + tr);

			for (int i = 2; i < a; i = i + 4) {

				p[i] = intarray[i] + tp;
				tp = p[i];
				// t[i] = arr[i]+t[i];
				// System.out.println("value of array : "+p[i]);
			}
			System.out.println("value of array tp: " + tp);

			for (int i = 3; i < a; i = i + 4) {

				f[i] = intarray[i] + tf;
				tf = f[i];
				// t[i] = arr[i]+t[i];
				// System.out.println("value of array : "+f[i]);
			}
			System.out.println("value of array tf: " + tf);
		} else {
			// There are no elements inside it.
			System.out.println("summary array is null" + Arrays.toString(strs));
			str += "<tr><td> Summary Array is empty <td><tr>";
		}

		///////////////////////////////////////// Failed table
		///////////////////////////////////////// code///////////////////////////////////////////////////////////////////////
		if (strs2 != null && strs2.length > 0) {
			// This means there are some elements inside name array.
			System.out.println("fail/pass array has elements" + Arrays.toString(strs));
			for (String k : strs2) {
				String[] k2 = k.split(":");
				/*
				 * for(String results : k2) { // System.out.println(results); }
				 */
				str1 += "<tr><td>" + k2[0] + "</td><td>" + k2[1] + "</td></tr>";
			}
			System.out.println(str1);

		} else {
			// There are no elements inside it.
			System.out.println("fail/pass array is null" + Arrays.toString(strs2));
			str1 += "<tr><td> No Fail Testcase <td><tr>";
		}

		// Step1
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");

		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		System.out.println("\n\n adding emails in Session..");
		
		
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("yayati.moongrey@hungama.com"));


		System.out.println("\n\n mails added in Session..");
		generateMailMessage.setSubject("::: Automation report ::: ");

		

		String emailBody = "<p>Hi Team,</p>"
				
				+ "Please find the below test report of WEBSITE as on "
				+ formatter.format(date) + "<br>" + "<br>"
				
				+ "Total No. of Automated Testcases	:" + tt + "<br>\n" + "Running Testcases					:" + tr
				+ "<br>\n" + "Pass Testcases						:" + tp + "<br>\n"
				+ "Fail Testcases						:" + tf + "<br>\n"

				+ "<div id=\"div1\" width=\"100%\">" + "<p>&nbsp;</p>"

				+ "<table width=\"55%\" align=\"left\" height: 39px width: 250px border=2>" + "<tbody>" + "<tr>"
				+ "<td style=\"width: 40px;\">TC_Module</td>" + "<td style=\"width: 50px;\">Description</td>"
				+ "<td style=\"width: 30px;\">Count</td>" + "</tr>" + str + "</tbody>" + "</table>"

				+ "<table width=\"35%\" align=\"right\" height: 39px width: 200px border= 2>" + "<tbody>" + "<tr>"
				+ "<td style=\"width: 180px;\">Failed TC_Name</td>" + "<td style=\"width: 60px;\">Id</td>" + "</tr>"
				+ str1 + "</tbody>" + "</table>" + "<br>" + "<p>&nbsp" + "</div>" + "<br>";
		

		generateMailMessage.setContent(emailBody, "text/html");
		System.out.println("Mail Session has been created successfully..");

		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");

		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		transport.connect("smtp.gmail.com", "hungama.qa123@gmail.com", "Hungama@123");
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
	}

	

	@Test
	public void f() throws AddressException, MessagingException, Exception, IOException {

		String[] Search_summary = ResultArray.ArrayPrint1(Search.excel);
		String[] Search_pass = ResultArray.ArrayPrint2(Search.excel);
		String[] Search_fail = ResultArray.ArrayPrint3(Search.excel);
		
				
		// Joint Result Summary
		String[] result = joinArrayGeneric( Search_summary);

		// Joint Result Fail
		String[] resultFail = joinArrayGeneric(Search_fail);
		

		System.out.println(Arrays.toString(result));

		generateAndSendEmail(result, resultFail);
		
		System.out.println("\n\n ===> Your Java Program has just sent an Email successfully. Check your email..");
	}

	private <T> T[] joinArrayGeneric(T[]... arrays) {
		// TODO Auto-generated method stub
		int length = 0;
		for (T[] array : arrays) {
			length += array.length;
		}

		// T[] result = new T[length];
		final T[] result = (T[]) Array.newInstance(arrays[0].getClass().getComponentType(), length);

		int offset = 0;
		for (T[] array : arrays) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}

		return result;

	}

}