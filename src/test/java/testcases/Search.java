package testcases;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import config.DataProvider;
import io.appium.java_client.android.AndroidDriver;

import utils.CommonUtils;
import utils.ResultArray;
import utils.StartBrowser;
import xpaths.Path;

public class Search {
	
	WebDriver we;
	FileInputStream fi;
	FileOutputStream fo;
	XSSFWorkbook wb;
	XSSFSheet sh;
	public static String excelname = "Search.xlsx";
	public static String excel = CommonUtils.nameOfDirectory(excelname);
	Actions ac;
	String colorCd = "#000000";
	int TotalTC = 0, RunTC = 0, PassTC = 0, FailTC = 0, aa = 10, z = 0, f = 0, c = 0;
	String sheetname = "Search", name = "Search";
	String SearchPass[] = new String[aa];
	String SearchFail[] = new String[aa];
	JavascriptExecutor jse;

	// TotalTC for total no. of test cases
	// RunTC for total no. of running test cases
	// PassTC for total no. of pass test cases
	// FailTC for total no. of failed test cases
	public String Read_In(int r, int c) {
		return (sh.getRow(r).getCell(c).getStringCellValue());
	}

	public void Write_Into(String excel) throws Exception {
		Thread.sleep(500);
		fi.close();
		fo = new FileOutputStream(excel);
		wb.write(fo);
		Thread.sleep(300);
		fo.close();
	}

	@BeforeTest
	public void start() throws IOException, InterruptedException {

		fi = new FileInputStream(excel);
		wb = new XSSFWorkbook(fi);
		sh = CommonUtils.ReadExcel(fi, wb, sh, excel, sheetname);
		String BrowserNm;
		BrowserNm = Read_In(1, 5);
		System.out.println("Browser name : " + BrowserNm);
		we = StartBrowser.getBrowser(we, BrowserNm);
	}

	@AfterTest
	public void afterTest() throws Exception {

		System.out.println("final values ################# " + excel + name + Arrays.toString(SearchPass)
				+ Arrays.toString(SearchFail) + TotalTC + RunTC + PassTC + FailTC + f + z);
		ResultArray.end(we, excel, name, SearchPass, SearchFail, TotalTC, RunTC, PassTC, FailTC, f, z);
	}
	
	//SR_01	Verify Search icon Location
	@Test(priority = 0, enabled = true)
	public void SR_01() throws Exception {
		sh = CommonUtils.ReadExcel(fi, wb, sh, excel, sheetname);

		String srno, tcId, desc, sts, runTC, BrowserNm;
		c = c + 1;
		srno = Read_In(c, 0);
		tcId = Read_In(c, 1);
		runTC = Read_In(c, 4);

		if (runTC.equalsIgnoreCase("yes")) {

			RunTC++;
			System.out.println("Running Test case id : " + tcId);
			System.out.println("count of running test case : " + RunTC);
			try {
				
				System.out.println("============================ Test Case Started ============================");
				
				
				WebElement searchBox =we.findElement(By.xpath(Path.searchbox));
				System.out.println(" \n SEARCH textBox Found");
				
				int[] PassArry = ResultArray.passResult(we, sheetname, c, PassTC, tcId, SearchPass, z, excel, fi, wb,
						sh);
				PassTC = PassArry[0];
				z = PassArry[1];

			} catch (Exception s) {
				int[] FailArry = ResultArray.failResult(we, sheetname, c, FailTC, tcId, SearchFail, f, s, excel, fi, wb,
						sh);
				FailTC = FailArry[0];
				f = FailArry[1];
				TotalTC++;
				Write_Into(excel);Assert.fail();
			}
			Thread.sleep(500);
			Write_Into(excel);
			Thread.sleep(500);
			System.out.println(srno + "============================ Test Case Completed ============================");
		} else {
			System.out.println("Dont wanted to run this test case");
			throw new SkipException("Skipping / Ignoring - Script not Ready for Execution ");
		} 
		TotalTC++;
		System.out.println("Test case id : " + tcId);
		System.out.println("count : " + TotalTC);
	}
  

	//SR_02	verify if the ser clicked inside the Search keyword
	@Test(priority = 1, enabled = true)
	public void SR_02() throws Exception {
		sh = CommonUtils.ReadExcel(fi, wb, sh, excel, sheetname);

		String srno, tcId, desc, sts, runTC, BrowserNm;
		c = c + 1;
		srno = Read_In(c, 0);
		tcId = Read_In(c, 1);
		runTC = Read_In(c, 4);

		if (runTC.equalsIgnoreCase("yes")) {

			RunTC++;
			System.out.println("Running Test case id : " + tcId);
			System.out.println("count of running test case : " + RunTC);
			try {
				
				System.out.println("============================ Test Case Started ============================");
				
				we.get(DataProvider.BaseUrl);Thread.sleep(3000);
				
				WebElement searchBox =we.findElement(By.xpath(Path.searchbox));
				System.out.println(" \n SEARCH textBox Found");
				searchBox.click();searchBox.sendKeys("Shirts");
				Thread.sleep(1000);
				
				boolean staus_searchBox = searchBox.isEnabled();
				System.out.println("status of searchBox"+staus_searchBox);
				Assert.assertTrue(staus_searchBox);
				
				int[] PassArry = ResultArray.passResult(we, sheetname, c, PassTC, tcId, SearchPass, z, excel, fi, wb,
						sh);
				PassTC = PassArry[0];
				z = PassArry[1];

			} catch (Exception s) {
				int[] FailArry = ResultArray.failResult(we, sheetname, c, FailTC, tcId, SearchFail, f, s, excel, fi, wb,
						sh);
				FailTC = FailArry[0];
				f = FailArry[1];
				TotalTC++;
				Write_Into(excel);Assert.fail();
			}
			Thread.sleep(500);
			Write_Into(excel);
			Thread.sleep(500);
			System.out.println(srno + "============================ Test Case Completed ============================");
		} else {
			System.out.println("Dont wanted to run this test case");
			throw new SkipException("Skipping / Ignoring - Script not Ready for Execution ");
		} 
		TotalTC++;
		System.out.println("Test case id : " + tcId);
		System.out.println("count : " + TotalTC);
	}
  

	//SR_03	Verify if the user clicks on Search icon without giving any inputs
	@Test(priority = 2, enabled = true)
	public void SR_03() throws Exception {
		sh = CommonUtils.ReadExcel(fi, wb, sh, excel, sheetname);

		String srno, tcId, desc, sts, runTC, BrowserNm;
		c = c + 1;
		srno = Read_In(c, 0);
		tcId = Read_In(c, 1);
		runTC = Read_In(c, 4);

		if (runTC.equalsIgnoreCase("yes")) {

			RunTC++;
			System.out.println("Running Test case id : " + tcId);
			System.out.println("count of running test case : " + RunTC);
			try {
				
				System.out.println("============================ Test Case Started ============================");
				
				we.get(DataProvider.BaseUrl);Thread.sleep(3000);
				WebElement searchBox =we.findElement(By.xpath(Path.searchbox));
				System.out.println(" \n SEARCH textBox Found");
				
				WebElement searchSubmit =we.findElement(By.xpath(Path.search_submit_button));
				searchSubmit.click();Thread.sleep(3000);
				Thread.sleep(1000);
				
				WebElement searchResult_searchtext =we.findElement(By.xpath(Path.search_results_search_text_heading));
				System.out.println("SEARCH TEXT found after blank search...."+searchResult_searchtext.getText());
				
				WebElement searchResult_Count =we.findElement(By.xpath(Path.search_result_counts));
				System.out.println("------>>>>> Search Result counts ----->>>"+searchResult_Count.getText());
				
				
				WebElement eneterSearchKeyword =we.findElement(By.xpath(Path.search_please_enter_keyword_alert));
				System.out.println("Enter Search Keywaord Alert Found ");
				
				WebElement women =we.findElement(By.linkText(Path.category_women));
				WebElement dresses =we.findElement(By.xpath(Path.category_dresses));
				WebElement Tshirts =we.findElement(By.xpath(Path.category_T_shirts));
				System.out.println("\n WOMEN.....DRESSES.....TSHIRTS Found After Blank search");
				
				int[] PassArry = ResultArray.passResult(we, sheetname, c, PassTC, tcId, SearchPass, z, excel, fi, wb,
						sh);
				PassTC = PassArry[0];
				z = PassArry[1];

			} catch (Exception s) {
				int[] FailArry = ResultArray.failResult(we, sheetname, c, FailTC, tcId, SearchFail, f, s, excel, fi, wb,
						sh);
				FailTC = FailArry[0];
				f = FailArry[1];
				TotalTC++;
				Write_Into(excel);Assert.fail();
			}
			Thread.sleep(500);
			Write_Into(excel);
			Thread.sleep(500);
			System.out.println(srno + "============================ Test Case Completed ============================");
		} else {
			System.out.println("Dont wanted to run this test case");
			throw new SkipException("Skipping / Ignoring - Script not Ready for Execution ");
		} 
		TotalTC++;
		System.out.println("Test case id : " + tcId);
		System.out.println("count : " + TotalTC);
	}
  


}
